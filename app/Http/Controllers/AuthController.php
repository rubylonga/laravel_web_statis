<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('form.register');
    }

    public function show(Request $request){
        $first = $request->first;
        $last = $request->last;
        $gender = $request->gender;
        $nation = $request->national;
        $spoken = $request->spoken;
        $bio = $request->bio;
        return view('form.welcome',compact('first','last','gender','nation','spoken','bio'));
    }
}
