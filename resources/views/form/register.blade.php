<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h4>Sign Up Form</h4>
    <form action="/welcome" method="POST">
        @csrf
        <label for="first">First name</label><br><br>
        <input type="text" name="first" id="first" required><br><br>
        <label for="last">Last Name</label><br><br>
        <input type="text" name="last" id="last" required><br><br>
        <label for="gender">Gender</label><br>
        <input type="radio" name="gender" value="man">
        <label for="man">Man</label><br>
        <input type="radio" name="gender" value="woman">
        <label for="woman">woman</label><br>
        <input type="radio" name="gender" value="other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality</label>
        <select name="national" id="nationality">
            <option value="1">Indonesia</option>
            <option value="2">Singapura</option>
            <option value="3">Malaysia</option>
            <option value="4">Thailand</option>
        </select><br><br>
        <label for="spoken">Languange Spoken</label><br>
        <input type="checkbox" name="spoken" value="indo" id="indo">
        <label for="indo">Bahasa Indonesia</label><br>
        <input type="checkbox" name="spoken" id="eng" value="eng">
        <label for="eng">English</label><br>
        <input type="checkbox" name="spoken" id="arab" value="arab">
        <label for="arab">Arabic</label><br>
        <input type="checkbox" name="spoken" id="japan" value="japan">
        <label for="japan">Japanase</label><br><br>
        <label for="bio">Bio</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <button type="submit">Simpan</button>
    </form>
</body>
</html>